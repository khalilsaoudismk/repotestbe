package com.example.testweborange.controller;

import com.example.testweborange.entities.Particulier;
import com.example.testweborange.service.ParticulierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ParticulierController {

    @Autowired
    private ParticulierService particulierService;
    @PostMapping("/ajouterParticulier")
    public ResponseEntity ajouterParticulier(@RequestBody Particulier particulier) throws Exception {
        Particulier particulierResult = particulierService.ajouterParticulier(particulier);
        return new ResponseEntity<>(particulierResult,HttpStatus.OK);
    }

    @GetMapping("/getALLParticulier")
    public ResponseEntity getALLParticulier() throws Exception {
        List<Particulier> particulierResultList = particulierService.getALLParticulier();
        return new ResponseEntity<>(particulierResultList,HttpStatus.OK);
    }

}

package com.example.testweborange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestweborangeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestweborangeApplication.class, args);
    }

}

package com.example.testweborange.service;

import com.example.testweborange.entities.Particulier;

import java.util.List;

public interface ParticulierService {
    Particulier ajouterParticulier(Particulier particulier);

    List<Particulier> getALLParticulier();
}

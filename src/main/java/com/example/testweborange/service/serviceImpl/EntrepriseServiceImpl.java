package com.example.testweborange.service.serviceImpl;

import com.example.testweborange.service.EntrepriseService;
import com.example.testweborange.service.ParticulierService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EntrepriseServiceImpl implements EntrepriseService {
}

package com.example.testweborange.service.serviceImpl;

import com.example.testweborange.entities.Particulier;
import com.example.testweborange.repository.ParticulierRepository;
import com.example.testweborange.service.ParticulierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ParticulierServiceImpl implements ParticulierService {

    @Autowired
    private ParticulierRepository particulierRepository;

    @Override
    public Particulier ajouterParticulier(Particulier particulier) {
        return particulierRepository.save(particulier);
    }

    @Override
    public List<Particulier> getALLParticulier() {
        return particulierRepository.findAll();
    }


}

package com.example.testweborange.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Entreprise {
    @Id
    @Column(unique = true)
    private String registreDeCommerce;
    private String nomEntreprise;
    private String nom;
    private String prenom;
    private String telephone1;
    private String telephone2;
    private String email;
    private String adresse;
    private String ville;
    private String pays;
}

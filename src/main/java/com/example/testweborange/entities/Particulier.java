package com.example.testweborange.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Particulier {
    @Id
    @Column(unique = true)
    private String adresseEmail;
    private String password;
    private String nom;
    private String prenom;
    private Date dateNaissance;
    private String adresse;
    private String ville;
    private String pays;
    private String niveau;
    private String profile;
    private String telephone;
}

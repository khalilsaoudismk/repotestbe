package com.example.testweborange.repository;

import com.example.testweborange.entities.Particulier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticulierRepository extends JpaRepository<Particulier,String>{
}

package com.example.testweborange.repository;

import com.example.testweborange.entities.Entreprise;
import com.example.testweborange.entities.Particulier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrepriseRepository extends JpaRepository<Entreprise,String>{
}
